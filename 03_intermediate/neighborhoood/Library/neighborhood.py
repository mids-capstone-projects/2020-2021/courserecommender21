
import pandas as pd
import numpy as np
from scipy import sparse
from sklearn.metrics.pairwise import cosine_similarity
import collections

class RecommendCourses:

    def __init__(self):
        self = None

    def read_data(self, filepath, k):
        data = pd.read_csv(filepath)
        self.df = data.groupby('Descr').filter(lambda x : len(x)>k)
        self.df_binary = pd.get_dummies(self.df.Descr).groupby(self.df['Calculation ID']).sum().reset_index()
        self.df_binary_noid = self.df_binary.drop('Calculation ID', 1)
        magnitude = np.sqrt(np.square(self.df_binary_noid).sum(axis=1))
        data_norm = self.df_binary_noid.divide(magnitude, axis='index')
        data_norm = pd.DataFrame(data_norm).fillna(0)
        return(data_norm)

    def cal_cosine(self, data_items) :
        data_sparse = sparse.csr_matrix(data_items)
        similarities = cosine_similarity(data_sparse.transpose())
        sim = pd.DataFrame(data=similarities, index= data_items.columns, columns= data_items.columns)
        return sim

    def eval(self, k, ID, n) :
        # k: semester number
        # ID: student's ID
        # n: the number of recommendation classes
        self.evalset = self.df[self.df['Class Year Number']== k ]
        wholeset = self.df[self.df['Class Year Number'] < k ]
        # Split train and test data
        df_binary_t = pd.get_dummies(wholeset.Descr).groupby(self.df['Calculation ID']).sum().reset_index()
        # drop 'ID'
        df_binary_noid_t = df_binary_t.drop('Calculation ID', 1)
        #normalize data
        magnitude_t = np.sqrt(np.square(df_binary_t).sum(axis=1))
        data_norm_t = df_binary_noid_t.divide(magnitude_t, axis='index')
        # calculate
        data_norm_t = pd.DataFrame(data_norm_t).fillna(0)
        distance_matrix_t = self.cal_cosine(data_norm_t)

        user_index = df_binary_t[df_binary_t['Calculation ID'] == ID].index.tolist()[0]
        known_user_likes = df_binary_noid_t.iloc[user_index]
        known_user_likes = known_user_likes[known_user_likes >0].index.values
        data_neighbours = pd.DataFrame(index=distance_matrix_t.columns, columns=range(1,11))
        for i in range(0, len(distance_matrix_t.columns)):
            data_neighbours.iloc[i,:10] = distance_matrix_t.iloc[0:,i].sort_values(ascending=False)[:10].index
        most_similar_to_likes = data_neighbours.loc[known_user_likes]
        similar_list = most_similar_to_likes.values.tolist()
        similar_list = list(set([item for sublist in similar_list for item in sublist]))
        neighbourhood = distance_matrix_t[similar_list].loc[similar_list]
        user_vector = df_binary_noid_t.iloc[user_index].loc[similar_list]
        score = neighbourhood.dot(user_vector).div(neighbourhood.sum(axis=1))
        score = score.drop(known_user_likes)
        return (score.nlargest(n))

    def acc_x(self, k, ID, n) :
        err=0
        cor=0

        b = self.eval(k, ID, n).index.to_numpy()
        a = self.evalset[self.evalset['Calculation ID']== ID]['Descr'].to_numpy()
        err_class = []
        cor_class = []
        for x in a :
            if x not in b :
                err = err + 1
                err_class.append(x)
            else :
                cor = cor + 1
                cor_class.append(x)
        return(cor/len(a), err_class, cor_class)

    def acc_total(self, k, n) :
        evalset = self.df[self.df['Class Year Number']== k ]
        evaldata = evalset['Calculation ID'].unique()
        self.acc_total = []
        for i in evaldata :
            self.acc_total.append(self.acc_x(k, i, n))
        score = [item[0] for item in self.acc_total]
        return(sum(score) / len(score))

    def acc_list(self, n) :
        acc_items = [item[2] for item in self.acc_total]
        acc_lists = sum(acc_items, [])
        acc_order = collections.Counter(acc_lists)
        return(acc_order.most_common(n))

    def err_list(self, n) :
        err_items = [item[1] for item in self.acc_total]
        err_lists = sum(err_items, [])
        err_order = collections.Counter(err_lists)
        return(err_order.most_common(n))
