import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder


from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse

data1 = pd.read_csv('/Users/josephlee/Documents/2020 Duke Fall/Capstone/econ.csv')
data1['graduation_year'] = data1['Comp Term Descr'].astype(str).str[:5]
data1['graduation_year'] = data1['graduation_year'].astype('int64')

data1.head()

data = data1[(data1['Enrollment Start'] == 2005) & (data1['graduation_year'] == 2009)]

data.head()
data = data.rename(columns={"Calculation ID": "ID"})

# less frequent classes need to be removed

data['Descr_enrollment'].value_counts()
df = data.groupby('Descr_enrollment').filter(lambda x : len(x)>10)
df['Descr_enrollment'].value_counts()

#One hot encoding

df4 = pd.get_dummies(df.Descr_enrollment).groupby(df.ID).sum()
df5 = df4.reset_index()
df5.head()

## Item-based

data_items = df5.drop('ID', 1)
data_items
magnitude = np.sqrt(np.square(data_items).sum(axis=1))
magnitude

data_items = data_items.divide(magnitude, axis='index')
data_items

def calculate_similarity(data_items):
    """Calculate the column-wise cosine similarity for a sparse
    matrix. Return a new dataframe matrix with similarities.
    """
    data_sparse = sparse.csr_matrix(data_items)
    similarities = cosine_similarity(data_sparse.transpose())
    sim = pd.DataFrame(data=similarities, index= data_items.columns, columns= data_items.columns)
    return sim

data_matrix = calculate_similarity(data_items)
data_matrix.head()


## User based

data_neighbours = pd.DataFrame(index=data_matrix.columns, columns=range(1,11))
for i in range(0, len(data_matrix.columns)):
    data_neighbours.iloc[i,:10] = data_matrix.iloc[0:,i].sort_values(ascending=False)[:10].index
data_neighbours


user = 2760.65
user_index = df5[df5.ID == user].index.tolist()[0]
user_index

known_user_likes = data_items.iloc[user_index]
known_user_likes = known_user_likes[known_user_likes >0].index.values
known_user_likes

most_similar_to_likes = data_neighbours.loc[known_user_likes]
most_similar_to_likes

similar_list = most_similar_to_likes.values.tolist()
similar_list = list(set([item for sublist in similar_list for item in sublist]))
neighbourhood = data_matrix[similar_list].loc[similar_list]
neighbourhood

user_vector = data_items.iloc[user_index].loc[similar_list]
user_vector
score = neighbourhood.dot(user_vector).div(neighbourhood.sum(axis=1))
score


score = score.drop(known_user_likes)
print (known_user_likes)
print (score.nlargest(20))
